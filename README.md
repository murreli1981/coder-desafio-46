# coder-desafio-46

Framworks - Koa

## Estructura del proyecto:

```
.
├── package-lock.json
├── package.json
└── src
    ├── config
    │   └── index.js
    ├── controllers
    │   ├── employee.controller.js
    │   └── index.js
    ├── db
    │   └── db.json
    ├── index.js
    ├── routes
    │   ├── employee.routes.js
    │   ├── index.js
    │   └── koa.router.js
    ├── server
    │   ├── index.js
    │   └── koa.server.js
    └── services
        ├── employee.service.js
        └── index.js
```

##

## Inicio del server

el server se ejecuta corriendo `npm start` que se encargará de transpilar ts y correr node

## Paths:

- `[GET] /employees` lista todos los empleados
- `[GET] /employees/:id` busca un empleado por id
- `[POST] /employees` guarda un nuevo empleado
- `[PATCH] /employees/:id` actualiza uno o varios valores de un empleado
- `[DEL] /employees/:id` elimina un empleado

### Documentación

#### Postman

en la colección de postman: `resources/coder-house-backend.postman_collection.json`, carpeta: `coder-desafio-46` están los endpoints para probar el crud de `employees` para validarlo rapidamente hay unos empleados precargados.

🚨 Todos los endpoints están con el path `localhost:3000` 🚨
