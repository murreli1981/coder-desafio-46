const { employeeRouter } = require("../routes");
const koaServer = require("./koa.server");

module.exports = {
  koaServer: koaServer(employeeRouter),
};
