const { PORT } = require("../config");
const Koa = require("koa");
const koaServer = (router) => {
  return {
    start() {
      const app = new Koa();
      app.use(router.routes());
      app.listen(PORT, () => {
        console.info(`>> Koa server listening on port ${PORT}`);
      });
    },
  };
};

module.exports = koaServer;
