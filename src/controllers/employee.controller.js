const employeeController = (service) => {
  return {
    getAll(ctx, next) {
      ctx.body = service.listAll();
    },
    getById(ctx, next) {
      const id = ctx.params.id;
      ctx.body = service.getById(id);
    },
    create(ctx, next) {
      const employee = ctx.request.body;
      const data = service.create(employee);
      ctx.body = data;
    },
    update(ctx, next) {
      const id = ctx.params.id;
      const payload = ctx.request.body;
      const data = service.update(id, payload);
      ctx.body = data;
    },
    delete(ctx, next) {
      const id = ctx.params.id;
      const data = service.delete(id);
      ctx.body = data;
    },
  };
};

module.exports = employeeController;
