const employeeController = require("./employee.controller");
const services = require("../services");

module.exports = {
  employeeController: employeeController(services.messageService),
};
