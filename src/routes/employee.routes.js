const employeeRoutes = (router) => {
  return (controller) => {
    router
      .get("/", controller.getAll)
      .get("/:id", controller.getById)
      .post("/", controller.create)
      .put("/:id", controller.update)
      .delete("/:id", controller.delete);
    return router;
  };
};

module.exports = employeeRoutes;
