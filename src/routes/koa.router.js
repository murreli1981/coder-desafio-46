const Router = require("koa-router");
const koaBody = require("koa-body");

module.exports = {
  routerApp: new Router({ prefix: "/employees" }).use(koaBody()),
};
