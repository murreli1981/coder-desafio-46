const employeeRoutes = require("./employee.routes");
const { employeeController } = require("../controllers");
const { routerApp } = require("./koa.router");

module.exports = {
  employeeRouter: employeeRoutes(routerApp)(employeeController),
};
