const { v4: uuidv4 } = require("uuid");

const messageService = (db) => {
  return {
    listAll() {
      return db;
    },
    getById(id) {
      return db.find((empl) => empl.id === id);
    },

    create(employee) {
      let employeeToCreate = employee;
      employeeToCreate.id = uuidv4();
      db.push(employeeToCreate);
      return employeeToCreate;
    },

    update(id, payload) {
      let employeeUpdated;
      db = db.map((emp) => {
        if (emp.id === id) {
          employeeUpdated = { ...emp, ...payload };
          return employeeUpdated;
        } else return emp;
      });
      return employeeUpdated;
    },

    delete(id) {
      let employeeDeleted;
      db = db.filter((emp) => {
        if (emp.id === id) {
          employeeDeleted = emp;
          return;
        } else return emp;
      });
      return employeeDeleted;
    },
  };
};

module.exports = messageService;
