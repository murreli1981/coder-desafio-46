const messageService = require("./employee.service");
const db = require("../db/db.json");

module.exports = {
  messageService: messageService(db),
};
